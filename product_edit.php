<?php
require_once 'create_db/connect.php';

if (!empty($_GET['id'])){
    $id = $_GET['id'];
    $sql = "SELECT * FROM product WHERE id=".$id;
    $result = $pdoDB->query($sql);
    $result_obj = $result->fetchObject();

}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Edit product:</h2>

<form action="product_update.php" method="post">

    <label for="title">Title:</label> <br>
    <textarea name="title" id="title"><?=$result_obj->title?></textarea><br>

    <label for="price">Price:</label> <br>
    <textarea name="price" id="price"><?=$result_obj->price?></textarea><br>

    <label for="description">Description:</label> <br>
    <textarea name="description" id="description"><?=$result_obj->description?></textarea><br>

    <label for="type">Type:</label> <br>
    <textarea name="type" id="type"><?=$result_obj->type?></textarea><br>

    <input type="hidden" name="product_id" value="<?=$result_obj->id?>">
    <button type="submit">Update</button>

</form>


</body>
</html>
