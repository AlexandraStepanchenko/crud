<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'create_db/connect.php';

try{

    $sql = "SELECT id, title,  price, description, type  FROM product";//послать запрос
    $result = $pdoDB->query($sql);//ответ в виде объекта
    $resultArray = $result->fetchAll();
}catch (PDOException $exp){
    die("Ошибка при извлечении!". $exp->getMessage());
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
        <?php foreach ($resultArray as $product):?>

        <div>
                <a href="product_show.php?id=<?=$product['id']?>">
                    <?=$product['title']?> <br>
                    <?=$product['price']." ". "uah"?> <br>
                    <?=$product['description']?> <br>
                    <?=$product['type']?> <br>
                </a> <br>
                <a href="product_edit.php?id=<?=$product['id']?>">Edit</a>

                <form action="product_delete.php" method="post">

                    <input type="hidden" name="product_id" value="<?=$product['id']?>">
                    <button type="submit">Delete</button>

                </form>
            </div>

        <?php endforeach;?>

        <h2>To add a commodity :</h2>

        <form action="product_add.php" method="post">

            <label for="title">Title:</label> <br>
            <textarea name="title" id="title"></textarea><br>

            <label for="price">Price:</label> <br>
            <textarea name="price" id="price"></textarea><br>

            <label for="description">Description:</label> <br>
            <textarea name="description" id="description"></textarea><br>

            <label for="type">Type:</label> <br>
            <textarea name="type" id="type"></textarea><br>

            <button type="submit">Save</button>

        </form>

</body>
</html>


