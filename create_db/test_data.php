<?php
require_once 'create_db/connect.php';

try{
    $sql = 'INSERT INTO product SET 
            title = "Ноутбук Acer Extensa EX2519-C313 (NX.EFAEU.054) Black ",
           
            description = "Экран 15.6\'\' (1366x768) HD LED, матовый / Intel Celeron N3060 (1.6 - 2.48 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics 400 / Без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.4 кг / черный";
            
              
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO product SET 
            title = "Ноутбук Acer Extensa EX2519 (NX.EFAEU.054)",
           
            description = "Экран 15.6\'\' (1366x768) HD LED, матовый / Intel Celeron N3060 (1.6 - 2.48 ГГц)  / Intel HD Graphics 400 / Без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.4 кг / черный";
            
              
';
    $pdoDB->exec($sql);
    $sql = 'INSERT INTO product SET 
            title = "Мужские часы CASIO MQ-24-7BUL/7BLLGF",
             
            description = "Коллекция Standard Analogue - это наручные часы с аналоговой индикацией времени классического вида, с кожаными ремешками или с браслетами из нержавеющей стали.";
            
              
';
    $pdoDB->exec($sql);
    $sql = 'INSERT INTO product SET 
            title = "Samsung Galaxy S8 64GB Midnight Black",
             
            description = " Поддерживается установка двух SIM-карт или одной SIM-карты и карты памяти. Слот для второй SIM-карты совмещен со слотом для карты памяти.";
            
              
';
    $pdoDB->exec($sql);
}catch(PDOException $exp){
    die('Не удалось добавить запись'.$exp->getMessage());
}

echo 'vse ok!';