<?php
require_once 'connect.php';

//запрос для создания таблицы
try{
    $sqlQ = '
        CREATE TABLE product(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
          title TEXT,
          price INT(5) NOT NULL,
          description TEXT
      
          
        )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
        
    ';
    $pdoDB->exec($sqlQ);
}catch (PDOException $exp) {
    echo 'Таблица product не создана!<br>' . $exp->getMessage();
    die();
}