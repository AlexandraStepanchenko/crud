<?php
//соединение с БД
try {
    $pdoDB = new PDO('mysql:host=localhost;dbname=products', 'root', '');
    $pdoDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdoDB->exec("SET NAMES 'utf8'");
}catch (PDOException $exp){
    echo'Нет соединения с базой данных!<br>'.$exp->getMessage();
    die();
}